function butt1_click(){
    console.log("Click Me!!!")
}

function butt2_click(){
    var tag_names = document.getElementsByTagName("p")
    tag_names[1].innerHTML = "<b>Hello Tag </b>"
    console.log(tag_names)
    var class_names = document.getElementsByClassName("p-green")
    console.log(class_names)
    var q_tag_names = document.querySelectorAll("p")
    console.log(q_tag_names)
    var q_class_names = document.querySelectorAll(".p-green")
    console.log(q_class_names)
    var p_1 = document.getElementById("p-1")
    console.log(p_1)
    p_1.innerHTML = "Test"
    var q_p_1 = document.querySelector("#p-1")
    console.log(q_p_1)
}