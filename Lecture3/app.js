function f1(t){
    document.write(t)
    document.write("<br>")
    document.write(t[4])
    document.write("<br>")
    document.write(t.charAt(4))
    document.write("<hr>")
    document.write(t.indexOf("gramm", 12))
    document.write("<hr>")
    document.write(t.indexOf("gramm1"))
    document.write("<hr>")
    document.write(t.lastIndexOf("gramm"))
}

// f1("JavaScript gramm is a programmn language")

function f2(text, subText){  
    var count = 0, index = 0

    if(text.indexOf(subText)==0){
        count++
    }

    while(index!=-1){
        index = text.indexOf(subText, index+1)
        count++
    }
    return count-1
}

console.log(f2("dddgrammJavaScript gramm dsjknk gramm is a programmn languagegramm", "gramm"))