var ob = {
    name: "Beso",
    age: 23,
    education: ["eu", "tsu", "cu"],
    family: {mother: "Nino", sister: "Nia", child: "Nika"},
    printData: function (params) {
        console.log("THis is a function")
    }
}

console.log(ob)
console.log(ob.education[1])
console.log(ob.family.sister)
ob.printData()

function loadDoc() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var data = this.responseText;
        console.log(data);
        var jsOb = JSON.parse(data);
        console.log(jsOb);
        console.log(jsOb.title);
      }
    };
    xhttp.open("GET", "https://jsonplaceholder.typicode.com/posts/1");
    xhttp.send();
  }

  loadDoc();